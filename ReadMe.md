Clone the project

git clone https://gitlab.com/sathyareddy/hoVehicleChk.git

Running SeleniumTest:

1. Navigate to the project directory ( cd vehicleDetails)
2. Run the below maven command to run the test

mvn test

NOTE: If maven is not installed, follow the below link for the guidance
https://maven.apache.org/install.html


Scanning various directories for CSV files:

Junit test has been added on FilePickerApplication.java
1. Run junit test or main method

