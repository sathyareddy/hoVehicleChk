package uk.gov.vehicle.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Navigate {

    private WebDriver driver;

    By startNowButton = By.linkText("Start now");

    public Navigate(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateTo(String url) {
        driver.navigate().to(url);
    }
}
