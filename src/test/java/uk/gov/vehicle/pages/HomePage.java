package uk.gov.vehicle.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private WebDriver driver;

    By startNowButton = By.linkText("Start now");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnStartNowButton() {
        driver.findElement(startNowButton).click();
    }
}
