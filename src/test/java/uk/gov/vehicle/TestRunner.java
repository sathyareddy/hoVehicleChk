package uk.gov.vehicle;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        plugin = {"pretty", "html:target/cucumber.html",
                "json:target/cucumber/report.json",
                "junit:target/cucumber.xml"},
        glue = {"classpath:uk.gov.vehicle/steps"},
        features = "classpath:uk.gov.vehicle/features" )
public class TestRunner {


}
