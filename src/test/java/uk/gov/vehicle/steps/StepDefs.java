package uk.gov.vehicle.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import uk.gov.vehicle.UtilPage;
import uk.gov.vehicle.exception.FilePickerException;
import uk.gov.vehicle.model.FileMetaData;
import uk.gov.vehicle.pages.CheckVehiclePage;
import uk.gov.vehicle.pages.HomePage;
import uk.gov.vehicle.pages.Navigate;
import uk.gov.vehicle.pages.VehicleSummaryPage;
import uk.gov.vehicle.service.FilePickerService;
import uk.gov.vehicle.service.FilePickerServiceImpl;

import javax.rmi.CORBA.Util;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class StepDefs extends UtilPage {

    public WebDriver driver;

    @Before
    public void setup() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        System.setProperty("webdriver.chrome.driver", "src/browserdrivers/chromedriver.exe");
        driver = new ChromeDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.close();
        driver.quit();
    }

    @Given("^I navigated to the vehicle information website \"([^\"]*)\"$")
    public void i_navigated_to_the_vehicle_information_website(String url) throws Throwable {
        Navigate navigate = new Navigate(driver);
        navigate.navigateTo(url);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnStartNowButton();
    }

    @When("^I enter the vehicle registration number and click on submit button$")
    public void i_enter_the_vehicle_registration_number_and_click_on_submit_button() throws Throwable {
        getVehicleDetails().forEach((regNo, details) -> {
            CheckVehiclePage checkVehiclePage = new CheckVehiclePage(driver);
            checkVehiclePage.enterRegNo(regNo);
            checkVehiclePage.clickOnContinue();
            VehicleSummaryPage vehicleSummaryPage = new VehicleSummaryPage(driver);
            assertThat(vehicleSummaryPage.getMake(), is(equalTo(getMake(details))));
            assertThat(vehicleSummaryPage.getColor(), is(equalTo(getColor(details))));
            vehicleSummaryPage.clickOnBackLink();
        });
    }
}
