package uk.gov.vehicle.exception;

public class FilePickerException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private String message = null;

	public FilePickerException() {
		super();
	}

	public FilePickerException(String message) {
		super(message);
		this.message = message;
	}

	public FilePickerException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}
	
}