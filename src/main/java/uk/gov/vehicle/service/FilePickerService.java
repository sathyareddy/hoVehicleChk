package uk.gov.vehicle.service;

import java.util.List;

import uk.gov.vehicle.exception.FilePickerException;
import uk.gov.vehicle.model.FileMetaData;

public interface FilePickerService {
	
	List<FileMetaData> getFileByMimeType(String[] mimeType, String dirPath) throws FilePickerException;

}
