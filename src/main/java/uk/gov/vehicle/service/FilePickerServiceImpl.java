package uk.gov.vehicle.service;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import uk.gov.vehicle.exception.FilePickerException;
import uk.gov.vehicle.model.FileMetaData;
import uk.gov.vehicle.model.FileType;
import uk.gov.vehicle.util.FileUtils;

public class FilePickerServiceImpl implements FilePickerService {

	List<FileMetaData> fileMetaDataList = new ArrayList<FileMetaData>();

	@Override
	public List<FileMetaData> getFileByMimeType(String[] mimeTypes, String dirPath) throws FilePickerException {
		fileMetaDataList = new ArrayList<FileMetaData>();

		try {
			search(new File(dirPath), mimeTypes , fileMetaDataList);
		} catch (IOException e) {
			throw new FilePickerException("An Error occured");
		}

		return fileMetaDataList;
	}
	
	private void search(File file, String[] mimeTypes, List<FileMetaData> fileMetaDataList) throws MalformedURLException, IOException {
		if (file.isDirectory()) {
		  System.out.println("Searching directory ... " + file.getAbsoluteFile());
		  // mimeTypes - parameters is optional - currenlty we deal with csv and xls only
	        //do you have permission to read this directory?
		    if (file.canRead()) {
				for (File temp : file.listFiles()) {
				    if (temp.isDirectory()) {
				    	search(temp, mimeTypes, fileMetaDataList);
				    } else {
				    	
				    	String mimeType = FileUtils.getMimeType(temp);
				    	// only csv and xls are covered at the moment
						if (FileType.CSV.name().equalsIgnoreCase(mimeType.split("/")[1]) 
								|| FileType.EXCEL.name().equalsIgnoreCase(mimeType.split("/")[1])) {
							FileMetaData fileMetaData = new FileMetaData();
							fileMetaData.setName(temp.getName());
							fileMetaData.setSize("kilobytes : "+temp.getTotalSpace()/1024);
							fileMetaData.setExtention(mimeType.split("/")[1]);
							fileMetaData.setMimeType(mimeType);
							fileMetaDataList.add(fileMetaData);
					    }
				    }
				}
			 } else {
				System.out.println(file.getAbsoluteFile() + "Permission Denied");
			 }
	      }
	  }
	
}
