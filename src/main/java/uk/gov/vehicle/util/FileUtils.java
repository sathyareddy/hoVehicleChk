package uk.gov.vehicle.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;

import javax.activation.MimetypesFileTypeMap;

public class FileUtils {
	
	public static String getMimeType(File file)
			throws java.io.IOException, MalformedURLException {
		try
	    {
			if (null == Files.probeContentType(file.toPath())) {
				final MimetypesFileTypeMap mtftp = new MimetypesFileTypeMap();
				mtftp.addMimeTypes("text/xml xml XML");
				mtftp.addMimeTypes("text/csv csv");
			    final String mimetype = mtftp.getContentType(file);
				return mimetype;
			}
			return Files.probeContentType(file.toPath());
	    }
	    catch (IOException ioException)
	    {
	        System.out.println(
	                "ERROR: Unable to determine file type for " + file.getName()
	                        + " due to exception " + ioException);
	    }
		return null;
	}
	
}
