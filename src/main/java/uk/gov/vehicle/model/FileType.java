package uk.gov.vehicle.model;

public enum FileType {

	CSV("csv", 1), EXCEL("xls", 2), TXT("txt", 3), DOC("doc", 4);

	private String mimeType;

	private int idx;

	FileType(String mimeType, int idx) {
		this.mimeType = mimeType;
		this.idx = idx;
	}

	public static FileType getFileType(String mimeType) {
		if (mimeType != null) {
			for (FileType g : FileType.values()) {
				if (mimeType.equalsIgnoreCase(g.mimeType)) {
					return g;
				}
			}
		}
		return null;
	}

	FileType getFileType() {
		return FileType.valueOf(mimeType);
	}

	int getIdx() {
		return idx;
	}

}
