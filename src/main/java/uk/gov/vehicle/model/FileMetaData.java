package uk.gov.vehicle.model;

public class FileMetaData {
	String name;
	
	String mimeType;
	
	String size;
	
	String extention;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getExtention() {
		return extention;
	}

	public void setExtention(String extention) {
		this.extention = extention;
	}

	@Override
	public String toString() {
		return "FilePicked [name=" + name + ", mimeType=" + mimeType
				+ ", size=" + size + ", extention=" + extention + "]";
	}
	
}
